import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import javax.json.*;
import java.io.IOException;
import java.io.StringReader;

public class JsonRpcClient {

    private String login;
    private String password;
    private String ip;
    private String port;

    public JsonRpcClient(String login, String password, String ip, String port) {
        this.login = login;
        this.password = password;
        this.ip = ip;
        this.port = port;
    }

    public JsonObject request(String method, JsonArray params) throws IOException {
        String url = "http://" + this.login + ":" + this.password + "@" + this.ip + ":" + this.port + "/";


        JsonObjectBuilder builder = Json.createObjectBuilder();

        builder.add("method", method);

        if (params != null) {
            builder.add("params", params);
        }

        JsonObject requestData = builder.build();

        //System.out.println(requestData.toString());

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpPost request = new HttpPost(url);
        request.addHeader("content-type", "application/json");
        request.setEntity(new StringEntity(requestData.toString()));

        HttpResponse response = httpClient.execute(request);

        String json_string = EntityUtils.toString(response.getEntity());
        JsonReader reader = Json.createReader(new StringReader(json_string));
        JsonObject responceData = reader.readObject();
        reader.close();

        return responceData;
    }

}
