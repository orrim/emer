import org.apache.commons.codec.binary.Base64;
import utils.Encryption;
import utils.FileUtils;
import utils.GZip;

import javax.json.*;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileService {

    private final String rpcUser = "emercoinrpc";
    private final String rpcPassword = "Cu9i9N73mgeDcsHcBrMD1mkvN1dAFbH1RbwRUcH5okUV";
    private final String rpcIp = "127.0.0.1";
    private final String rpcPort = "8775";

    private final String serviceId = "fs";

    private JsonRpcClient client = new JsonRpcClient(rpcUser, rpcPassword, rpcIp, rpcPort);

    public JsonObject addFileDirect(String filePath, int days) throws IOException {
        Path path = Paths.get(filePath);

        String name = this.serviceId + ":" + path.getFileName().toString();

        JsonArray params = Json.createArrayBuilder()
                .add(name)
                .add(filePath)
                .add(days)
                .add("")
                .add("filepath")
                .build();

        return client.request("name_new", params);
    }

    public JsonObject getFileDirect(String fileName, String destinationPath) throws IOException {
        JsonArray params = Json.createArrayBuilder()
                .add(this.serviceId + ":" + fileName)
                .add("")
                .add(destinationPath)
                .build();

        return client.request("name_show", params);
    }

    public JsonObject addFile(String filePath, int days, String password) throws Exception {
        Path path = Paths.get(filePath);

        String name = serviceId + ":" + path.getFileName().toString();

        byte[] data = Encryption.encrypt(password, GZip.compress(FileUtils.readFile(filePath)));
        String value = Base64.encodeBase64String(data);

        JsonArray params = Json.createArrayBuilder()
                .add(name)
                .add(value)
                .add(days)
                .add("")
                .add("base64")
                .build();

        return client.request("name_new", params);
    }

    public JsonObject getFile(String fileName, String destinationPath, String password) throws Exception {
        JsonArray params = Json.createArrayBuilder()
                .add(this.serviceId + ":" + fileName)
                .add("base64")
                .build();

        JsonObject responce = client.request("name_show", params);

        JsonValue error = responce.get("error");
        if (error.getValueType() != JsonValue.ValueType.NULL) {
            return responce;
        }

        JsonObject result = responce.getJsonObject("result");

        byte[] data = Base64.decodeBase64(result.get("value").toString());
        FileUtils.writeFile(destinationPath, GZip.decompress(Encryption.decrypt(password, data)));

        return responce;
    }
}
