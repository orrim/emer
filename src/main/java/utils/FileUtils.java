package utils;

import java.io.*;

public class FileUtils {
    public static byte[] readFile(String filePath) throws IOException {
        File file = new File(filePath);
        int length = (int) file.length();

        BufferedInputStream reader = new BufferedInputStream(new FileInputStream(filePath));
        byte[] data = new byte[length];
        reader.read(data, 0, length);
        reader.close();

        return data;
    }

    public static void writeFile(String filePath, byte[] data) throws IOException {
        BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(filePath));
        writer.write(data);
        writer.flush();
        writer.close();
    }
}
