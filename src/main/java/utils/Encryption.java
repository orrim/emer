package utils;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Arrays;

public class Encryption {
    public static byte[] encrypt(String password, byte[] data) throws Exception {
        byte[] keyBytes = Arrays.copyOf(password.getBytes("ASCII"), 16);

        SecretKey key = new SecretKeySpec(keyBytes, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

        // build the initialization vector.  This example is all zeros, but it
        // could be any value or generated using a random number generator.
        byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        IvParameterSpec ivspec = new IvParameterSpec(iv);

        cipher.init(Cipher.ENCRYPT_MODE, key, ivspec);

        return cipher.doFinal(data);
    }

    public static byte[] decrypt(String password, byte[] data) throws Exception {
        byte[] keyBytes = Arrays.copyOf(password.getBytes("ASCII"), 16);

        SecretKey key = new SecretKeySpec(keyBytes, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

        // build the initialization vector.  This example is all zeros, but it
        // could be any value or generated using a random number generator.
        byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        IvParameterSpec ivspec = new IvParameterSpec(iv);

        cipher.init(Cipher.DECRYPT_MODE, key, ivspec);

        return cipher.doFinal(data);
    }
}
