package utils;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GZip {
    public static byte[] compress(byte[] data) throws IOException {
        ByteArrayOutputStream byteOS = new ByteArrayOutputStream(data.length);

        try {
            GZIPOutputStream gos = new GZIPOutputStream(byteOS);
            try {
                gos.write(data);
            } finally {
                gos.close();
            }
        } finally {
            byteOS.close();
        }

        return byteOS.toByteArray();
    }

    public static byte[] decompress(byte[] data) throws IOException {
        ByteArrayInputStream byteIS = new ByteArrayInputStream(data);
        ByteArrayOutputStream byteOS = new ByteArrayOutputStream();

        byte[] buffer = new byte[1024];

        try {
            GZIPInputStream gis = new GZIPInputStream(byteIS);

            try {
                int len;
                while ((len = gis.read(buffer)) > 0) {
                    byteOS.write(buffer, 0, len);
                }
            } finally {
                gis.close();
            }
        } finally {
            byteOS.close();
        }

        return byteOS.toByteArray();
    }
}
