import javax.json.JsonObject;
import java.io.IOException;

public class Main {

    private final static String rpcUser = "emercoinrpc";
    private final static String rpcPassword = "fake_password";
    private final static String rpcIp = "127.0.0.1";
    private final static String rpcPort = "8775";

    public static void main(String[] args) throws Exception {

        JsonRpcClient client = new JsonRpcClient(rpcUser, rpcPassword, rpcIp, rpcPort);
        JsonObject res;

        //res = client.request("getinfo", null);
        //System.out.println(res.toString());

        FileService emerFile = new FileService();

        // Add file by path
        //res = emerFile.addFileDirect("D:\\test.txt", 1);
        //System.out.println(res.toString());

        // Read file by path
        //res = emerFile.getFileDirect("tst_test.txt","D:\\test_res.txt");
        //System.out.println(res.toString());

        // Add file with compression and encryption
        //res = emerFile.addFile("D:\\test3.txt", 5, "123456789");
        //System.out.println(res.toString());
        //{"result":"83cae7ac253672e879854ff66239aac3fb20d7a74058eede7d42205a8ce66f93","error":null,"id":null}

        // Read file with compression and encryption
        res = emerFile.getFile("test3.txt","D:\\test3_res.txt", "123456789");
        System.out.println(res.toString());


    }
}
